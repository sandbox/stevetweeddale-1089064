<?php

/**
 * Implements hook_init().
 */
function code_prettify_init() {
  drupal_add_library('code_prettify', 'google_code_prettify', TRUE);
}

/**
 * Implements hook_library().
 */
function code_prettify_library() {
  $libraries['google_code_prettify'] = array(
    'title' => 'Google-code-prettify',
    'website' => 'http://code.google.com/p/google-code-prettify/',
    'version' => '21-jul-2010',
    'js' => array(
      libraries_get_path('google-code-prettify') . '/src/prettify.js' => array('group' => JS_LIBRARY, 'weight' => 0),
      drupal_get_path('module', 'code_prettify') . '/code_prettify.js' => array('group' => JS_DEFAULT, 'weight' => 0),
    ),
    'css' => array(
      libraries_get_path('google-code-prettify') . '/src/prettify.css' => array('group' => CSS_DEFAULT),
    ),
  );

  return $libraries;
}

/**
 * Implements hook_filter_info().
 */
function code_prettify_filter_info() {
  $filters['code_prettify'] = array(
    'title' => t('Code prettify'),
    'description' => t('Adds a "pretty-print" class to code blocks, to trigger syntax highlighting.'),
    'process callback' => '_code_prettify_process',
    'tips callback' => '_code_prettify_tips',
  );
  return $filters;
}

/**
 * Filter process callback.
 */
function _code_prettify_process($text) {
  if (empty($text)) {
    return $text;
  }
  // @TODO investigate querypath usage here - this seems a bit dirty!
  $dom = new DOMDocument();
  $dom->loadHTML($text);
  $code_tags = $dom->getElementsByTagName('pre');
  foreach ($code_tags as $tag) {
    $tag->setAttribute('class', 'prettyprint');
  }
  // Strip out the wrapping page tags returned by $dom->saveHTML().
  $text = preg_replace('/^<!DOCTYPE.+?>/', '', str_replace( array('<html>', '</html>', '<body>', '</body>'), array('', '', '', ''), $dom->saveHTML()));

  return $text;
}

/**
 * Filter tips callback.
 */
function _code_prettify_tips($filter, $format, $long) {
  return t('Adds a class of "pretty-print" to all code tags.');
}
